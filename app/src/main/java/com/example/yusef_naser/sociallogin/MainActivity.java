package com.example.yusef_naser.sociallogin;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final int RC_SIGN_IN = 1010;
    TextView textView ;
    ImageView imageView ;
    CallbackManager callbackManager;
    GoogleApiClient mGoogleApiClient ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textView);
        imageView = (ImageView) findViewById(R.id.imageView);



        logInFaceBook();
        logInGooglePlus();


    }


    private void logInFaceBook(){
        callbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("public_profile");
        loginButton.setReadPermissions("email");
        //  LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                AccessToken accessToken = loginResult.getAccessToken();
                if(accessToken.isExpired()){
                    Toast.makeText(MainActivity.this, "Expired", Toast.LENGTH_SHORT).show();
                }else{
                    //  Toast.makeText(MainActivity.this, "not Expired", Toast.LENGTH_SHORT).show();
                    // Toast.makeText(MainActivity.this, "Expired Date is : \n"+accessToken.getExpires().toString() , Toast.LENGTH_SHORT).show();
                    //  textView.setText(accessToken.getToken());

                    GraphRequest request = GraphRequest.newMeRequest(
                            accessToken,
                            new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(JSONObject object, GraphResponse response) {
                                    try {
                                        String email = object.getString("email");
                                        String id = object.getString("id");
                                        String name = object.getString("name");
                                        String first_name = object.getString("first_name");
                                        String last_name = object.getString("last_name");
                                        String gender = object.getString("gender");

                                        textView.setText("name : "+name+"\n id : "+id+"\n first name : "
                                                +first_name+"\n last name : "+last_name+"\n email : "+email+"\n gender : "+gender);

                                        String imageUrl = "https://graph.facebook.com/" + id + "/picture?type=large";

                                        Glide.with(MainActivity.this).load(imageUrl).into(imageView);


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                            });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,name,first_name,last_name,gender,email");
                    request.setParameters(parameters);
                    request.executeAsync();


                    // "https://graph.facebook.com/" + userID + "/picture?type=large"

                }


            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        // loginButton.unregisterCallback(callbackManager);

    }

    private void logInGooglePlus(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Plus.API)
                .build();

        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setOnClickListener(this);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;

        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String personName = acct.getDisplayName();
            String personGivenName = acct.getGivenName();
            String personFamilyName = acct.getFamilyName();
            String personEmail = acct.getEmail();
            String personId = acct.getId();
            Uri personPhoto = acct.getPhotoUrl();


//            Person person  = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
//            Log.i(TAG, "--------------------------------");
//            Log.i(TAG, "Display Name: " + person.getDisplayName());
//            Log.i(TAG, "Gender: " + person.getGender());
//            Log.i(TAG, "AboutMe: " + person.getAboutMe());
//            Log.i(TAG, "Birthday: " + person.getBirthday());
//            Log.i(TAG, "Current Location: " + person.getCurrentLocation());
//            Log.i(TAG, "Language: " + person.getLanguage());


            textView.setText("name : "+personName+"\n id : "+personId+"\n first name : "
                    +personName+"\n last name : "+personFamilyName+"\n email : "+personEmail+"\n gender : "+personGivenName);



            Glide.with(MainActivity.this).load(personPhoto).into(imageView);


        } else {
            // Signed out, show unauthenticated UI.

        }
    }



}
